Everything we do is connected to the pursuit of bettering our communities and our fellow humankind. Through innovative seed-to-sale cannabis processes, we seek to help others live life on their terms through the natural power of cannabis. Call (786) 598-2022 for more information!

Address: 6827 Bird Rd, Miami, FL 33155, USA

Phone: 786-598-2022

Website: https://www.libertyhealthsciences.com